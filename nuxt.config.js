import colors from 'vuetify/es5/util/colors'
import dotenv from "dotenv";

dotenv.config();

export default {
  server: {
    host: "0.0.0.0",
    port: 3000,

  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - BookMe',
    title: 'BookMe',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0'},
      {hid: 'description', name: 'description', content: 'The best place to buy tickets for your favourite events'},
      {hid: "mobile-web-app-capable", name: "mobile-web-app-capable", content: "yes"},
      {hid: "apple-mobile-web-app-title", name: "apple-mobile-web-app-title", content: "BookMe"},
      {hid: "og:type", name: "og:type", property: "og:type", content: "webapp"},
      {hid: "og:title", name: "og:title", property: "og:title", content: "BookMe"},
      {hid: "og:site_name", name: "og:site_name", property: "og:site_name", content: "BookMe"}, {
        hid: "og:description",
        name: "og:description",
        property: "og:description",
        content: "The best place to buy tickets for your favourite events. Fast and secure!"
      },
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: "manifest", href: "/manifest.json"},
      {hid: "shortcut-icon", rel: "shortcut-icon", href: "/icon_64x64.png"},
      {hid: "apple-touch-icon", rel: "apple-touch-icon", href: "/icon_512x512.png", sizes: "512x512"}

    ],
    script: [
      {src: "https://www.paypal.com/sdk/js?client-id=ASYMWLf69U7OH6DcfyWTIxLdf-LiXGbvGhk4Wdb7WxPDQ7bkPH6pSBcOivGq6G0Y0KYUN-7q4c9nShQf&currency=DKK"}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/global.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: "~/plugins/vpaypalcheckout.js", ssr: false}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/auth',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.NODE_ENV !== 'production' ? `http://${process.env.development_ip}/api` : `http://${process.env.production_ip}/api`
  },
  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/'
    },
    cookie: {
      options: {
        maxAge: 7200
      }
    },
    strategies: {
      local: {
        endpoints: {
          login: {url: 'account/login', method: 'post', propertyName: 'token.token'},
          user: {url: 'account', method: 'get', propertyName: 'user'},
          logout: false
        },
      }
    }
  },


  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    vendor: [
      'v-paypal-checkout'
    ],
  }
}
